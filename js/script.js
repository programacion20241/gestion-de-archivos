document.querySelector("#archivo1").addEventListener("change", leerArchivo, false);

function leerArchivo(e){
    let archivo = e.target.files[0];
    if (!archivo) {
        return;
    }
    let lector = new FileReader(); //API DE JS QUE VA A LEER EL CONTENIDO, CREAMOS UN OBJETO QUE SE LLAMA LECTOR
    lector.readAsText(archivo);
    lector.onload = function(e){
        let contenido = e.target.result;
        mostrarContenido(contenido);
    }
}
function mostrarContenido(contenido){
    let parrafo = document.querySelector("#parrafo");
    parrafo.innerHTML = contenido;
}

//PROCESO PARA LEER LECTURA DE ARCHIVO CSV
document.querySelector("#archivo2").addEventListener("change", leerArchivo2, false);

function leerArchivo2(e) {
    let archivo = e.target.files[0];
    if (!archivo){
        return;
    }
    let lector = new FileReader(); //API DE JS QUE VA A LEER EL CONTENIDO, CREAMOS UN OBJETO QUE SE LLAMA LECTOR
    lector.readAsText(archivo);
    lector.onload = function(e){
        let contenido = e.target.result;
        mostrarTabla(contenido);
    }
}

function mostrarTabla(contenido){
    let tabular = document.querySelector("#tabular");
    let filas = contenido.split(/\r?\n|\r/);
    //TEMPLATE LITERAL
    let template = `<table>`;
    for (let i = 0; i < filas.length; i++) {
        let celdasFila = filas[i].split(',');
        if(i == 0){
            template += `<tr><th>${celdasFila[0]}</th><th>${celdasFila[1]}</th><th>${celdasFila[2]}</th><th>${celdasFila[3]}</th></tr>`
        }else{
            template += `<tr><td>${celdasFila[0]}</td><td>${celdasFila[1]}</td><td>${celdasFila[2]}</td><td><img src="Imagenes/${celdasFila[3]}"></td></tr>`
        }
    }
    template += `</table>`;
    tabular.innerHTML = template;
}

//PROCESO PARA EXPORTAR TEXTO A ARCHIVO

// Un objeto Blob representa un objeto tipo fichero de datos planos inmutables. Los Blobs representan datos que no necesariamente se encuentran en un formato nativo de JavaScript.

// Los MIME Types (Multipurpose Internet Mail Extensions) son las manera standard de mandar contenido a través de la red. Los tipos MIME especifican tipos de datos, como por ejemplo texto, imagen, audio, etc. que los archivos contienen.

// Dos tipos primarios de MIME son importantes para el papel de los tipos por defecto:

// text/plain es el valor predeterminado para archivos de texto. Un archivo de texto de ser legible por humanos y no debe contener datos binarios.
// Application/octet-stream es el valor predeterminado para todos los demás casos.

document.querySelector("#exportar").setAttribute("onclick", "exportarArchivo()");

function exportarArchivo(){
    let texto = document.querySelector("#texto").value;
    let nombreArchivo = "textogenerado.txt";
    blob = new Blob([texto], {type: "octet/stream"});
    url = window.URL.createObjectURL(blob);
    //CREAR UN ELEMENTO DE ENLACE
    let a = document.createElement("a");
    a.href = url;
    a.download = nombreArchivo;
    a.click();
    window.URL.revokeObjectURL(url);
}


